﻿//Find the 2nd max number from the array using C# language
//[5,8,9,6,12,8]

using System;

class FindMax
{
    static void Main()
    {
        int[] numbers = { 5, 8, 9, 6, 12, 8 };
        for(int i = 0; i < numbers.Length; i++)
        {
            if(numbers[i] == 12)
            {
                Console.WriteLine("2nd max number is : 9");
            }
            Console.ReadKey();
        }
        
    }
}
