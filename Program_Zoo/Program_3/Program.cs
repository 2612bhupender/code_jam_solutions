﻿using System;
namespace Problem
{
    class Zoos
    {
        public static void Main(string[] args)
        {
            int z_Count = 0, o_Count = 0;

            string word = Console.ReadLine();

            for (int word_Index = 0; word_Index < word.Length; word_Index++)
            {

                if (word[word_Index] == 'z')
                    o_Count++;

                else
                    z_Count++;
            }

            if (z_Count == (2 * o_Count))

                Console.WriteLine("Yes");
            else
                Console.WriteLine("No");

            Console.ReadLine();



        }
    }

}
