﻿using System;
public class Swapping_UsingThirdVariable
{
    public static void Main(string[] args)
    {
        int a_FirstValue;
        a_FirstValue = int.Parse(Console.ReadLine());
        int b_SecondValue;
        b_SecondValue = int.Parse(Console.ReadLine());
        int c_ThirdValue = 0;
        Console.WriteLine("Before swap a value is : " + a_FirstValue + " and b value is : " + b_SecondValue);
        
        c_ThirdValue = b_SecondValue;
        b_SecondValue = a_FirstValue;
        a_FirstValue = c_ThirdValue;
        Console.Write("After swap a value is : " + a_FirstValue + " and b value is : " + b_SecondValue);
    }
}
