using System;
public class Swapping
{
    public static void Main(string[] args)
    {
        int a_FirstValue;
        a_FirstValue = int.Parse(Console.ReadLine());
        int b_SecondValue;
        b_SecondValue = int.Parse(Console.ReadLine());
        Console.WriteLine("Before swap a value is : " + a_FirstValue + " and b value is : " + b_SecondValue);
        a_FirstValue = a_FirstValue + b_SecondValue;     
        b_SecondValue = a_FirstValue - b_SecondValue;       
        a_FirstValue = a_FirstValue - b_SecondValue; 
        Console.Write("After swap a value is : " + a_FirstValue + " and b value is : " + b_SecondValue);
    }
}
