﻿using System;

public class Isogram
{

    static bool is_isogram(string str)
    {
        str = str.ToLower();
        int len = str.Length;

        char[] arr = str.ToCharArray();

        Array.Sort(arr);
        for (int i = 0; i < len - 1; i++)
        {
            if (arr[i] == arr[i + 1])
                return false;
        }
        return true;
    }

    public static void Main()
    {
        string str1 = Console.ReadLine();
        Console.WriteLine(is_isogram(str1));
    }
}
